mod operations;
mod se4log;
mod siecle;

use clap::Parser;
use env_logger::{Builder, Env};
use operations::{logs_extract, LogExtractArgs};

#[derive(Parser)]
#[command(name = "siestsse4")]
#[command(bin_name = "siestsse4")]
enum Siestsse4Cli {
    #[command(name = "logextract")]
    LogsExtract(LogExtractArgs),
}

fn main() {
    // Prepare logger, using SIESTSSE4_LOG env variable
    let env = Env::default()
        .filter("SIESTSSE4_LOG_LEVEL")
        .write_style("SIESTSSE4_LOG_STYLE");

    Builder::from_env(env)
        .format_level(true)
        .format_timestamp_nanos()
        .init();

    // Read command line arguments
    let args: Siestsse4Cli = Siestsse4Cli::parse();

    // Launch the appropriate command
    match args {
        Siestsse4Cli::LogsExtract(args) => logs_extract(&args),
    }
}
