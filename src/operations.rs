use crate::{se4log, siecle};

#[derive(clap::Args)]
#[command(version, about, long_about = None)]
pub struct LogExtractArgs {
    #[clap(short, long)]
    siecle: String,
    #[clap(short, long)]
    se4log: String,
}

pub fn logs_extract(args: &LogExtractArgs) {
    log::info!("Decoding siecle xml file : {}", &args.siecle);
    let bee = siecle::decode(&args.siecle);
    log::info!("Decoding lines : {}", &args.se4log);
    let se4log = se4log::load_log(&args.se4log);
    let created = se4log::detect_created_lines(&se4log);
    log::info!(
        "Extracting passwords and logins : {} created",
        created.len()
    );
    println!("{:?}", created.len());
    let mut count = 0;
    let structures = bee.structures();
    for e in bee.eleves() {
        let estruct = structures.get(&e.eleve_id);
        if estruct.is_none() {
            continue;
        }
        let logins = [e.fname()];
        let mut found = false;
        for l in logins.iter() {
            if created.contains_key(l) {
                let c = created.get(l).unwrap();
                println!(
                    "{},{},{},{},{}",
                    estruct.unwrap().code_structure,
                    e.nom_de_famille,
                    e.prenom,
                    c.asked_login,
                    &c.password.as_ref().unwrap_or(&"????".to_string())
                );
                count += 1;
                found = true;
                break;
            }
        }
        if !found {
            println!(
                "{},{},{},????,????",
                estruct.unwrap().code_structure,
                e.nom_de_famille,
                e.prenom
            );
        }
    }
    log::info!("Done, {} logins found", count);
}
