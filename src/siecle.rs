//! This module loads and parses the XML files from SIECLE BEE.
//! The parsed structure is :
//!
//! ```text
//! BeeEleves
//! ├── Parametres
//! ├── Donnees
//! │   ├── Eleves
//! │   │   └── Vec<Eleve>
//! │   └── Structures
//! │       └── Vec<StructuresEleve>
//! ```

// use deunicode::deunicode;
use std::collections::HashMap;
use std::io::Read;
use std::{fs::File, io::BufReader};

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct BeeEleves {
    #[serde(rename = "@VERSION")]
    pub version: String,
    pub parametres: Parametres,
    pub donnees: Donnees,
}

impl BeeEleves {
    /// Returns the list of eleves
    pub fn eleves(&self) -> &[Eleve] {
        &self.donnees.eleves.eleves
    }

    /// Returns the the hashmap of structures by eleve_id
    pub fn structures(&self) -> HashMap<String, Structure> {
        self.donnees
            .structures
            .structures_eleves
            .iter()
            .fold(HashMap::new(), |mut acc, s| {
                acc.insert(s.eleve_id.clone(), s.structure.clone());
                acc
            })
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct Parametres {
    pub uaj: String,
    pub annee_scolaire: String,
    pub date_export: String,
    pub horodatage: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct Donnees {
    pub eleves: Eleves,
    pub structures: Structures,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct Eleves {
    #[serde(rename = "$value")]
    pub eleves: Vec<Eleve>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct Eleve {
    #[serde(rename = "@ELEVE_ID")]
    pub eleve_id: String,
    #[serde(rename = "@ELENOET")]
    pub elenoet: String,
    pub id_national: Option<String>,
    #[serde(rename = "ELENOET")]
    pub elenoet_2: String,
    pub id_eleve_etab: String,
    pub nom_de_famille: String,
    pub prenom: String,
    pub date_naiss: String,
    pub doublement: bool,
    pub code_pays: Option<usize>,
    pub accepte_sms: Option<bool>,
    pub date_modification: String,
    pub date_sortie: Option<String>,
    pub mel: Option<String>,
    pub code_regime: usize,
    pub date_entree: String,
    pub ville_naiss: Option<String>,
    pub code_motif_sortie: Option<usize>,
    pub code_sexe: usize,
    pub tel_portable: Option<String>,
    pub code_pays_nat: Option<usize>,
    pub code_statut: Option<String>,
    pub code_departement_naiss: Option<String>,
    pub code_commune_insee_naiss: Option<String>,
    pub adhesion_transport: bool,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct Structures {
    #[serde(rename = "$value")]
    pub structures_eleves: Vec<StructuresEleve>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct StructuresEleve {
    #[serde(rename = "@ELEVE_ID")]
    pub eleve_id: String,
    #[serde(rename = "@ELENOET")]
    pub elenoet: String,
    pub structure: Structure,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub struct Structure {
    pub code_structure: String,
    pub type_structure: String,
}

impl Eleve {
    // Possible implementations for login generation

    // pub fn login(&self) -> String {
    //     let fam = &self
    //         .nom_de_famille
    //         .to_lowercase()
    //         .replace("'", "")
    //         .replace(" ", "-");
    //     // Only keep the first part before -
    //     let fam = fam.split('-').next().unwrap();
    //     let fam = deunicode(fam);
    //     format!(
    //         "{}.{}",
    //         deunicode(&self.prenom.to_lowercase().replace(" ", "")),
    //         fam
    //     )
    // }

    // pub fn loginc(&self) -> String {
    //     let fam = &self
    //         .nom_de_famille
    //         .to_lowercase()
    //         .replace("'", "")
    //         .replace(" ", "");
    //     // Only keep the first part before -
    //     let fam = deunicode(fam);
    //     format!(
    //         "{}.{}",
    //         deunicode(&self.prenom.to_lowercase().replace(" ", "")),
    //         fam
    //     )
    // }

    // pub fn loginf(&self) -> String {
    //     let fam = &self
    //         .nom_de_famille
    //         .to_lowercase()
    //         .replace("'", "")
    //         .replace(" ", "-");
    //     let fam = deunicode(fam);
    //     format!(
    //         "{}.{}",
    //         deunicode(&self.prenom.to_lowercase().replace(" ", "")),
    //         fam
    //     )
    // }

    // pub fn loginn(&self, n: u8) -> String {
    //     format!("{}{}", self.login(), n)
    // }

    // pub fn loginnf(&self, n: u8) -> String {
    //     format!("{}{}", self.loginf(), n)
    // }

    // pub fn loginnc(&self, n: u8) -> String {
    //     format!("{}{}", self.loginc(), n)
    // }

    /// Returns the full name of the student (prenom nom_de_famille)
    pub fn fname(&self) -> String {
        format!("{} {}", self.prenom, self.nom_de_famille)
    }
}

/// Decode the XML file at the given path, returning a BeeEleves struct.
/// BEE xmls are encoded in ISO-8859-15, they need to be decoded before being parsed.
pub fn decode(path: &str) -> BeeEleves {
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let content: Vec<u8> = reader.bytes().map(|b| b.unwrap()).collect();
    let (decoded, _, _) = encoding_rs::ISO_8859_15.decode(&content);
    let bee: BeeEleves = quick_xml::de::from_str(&decoded).unwrap();
    bee
}
