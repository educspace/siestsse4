use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

#[allow(dead_code)]
pub struct CreatedLine {
    pub full_name: String,
    pub ent_login: String,
    pub asked_login: String,
    pub password: Option<String>,
}

/// Detect lines with this pattern :
///
/// ```text
/// Compte ENT {{fullname}} ({{ent_login}}) : nouvel utilisateur, login demandé : {{asked_login}}.
/// ```
///
/// and return a hashmap of `login => CreatedLine` with the extracted values.
///
pub fn detect_created_lines(lines: &[String]) -> HashMap<String, CreatedLine> {
    let passwords = read_passwords(lines);
    let mut created_lines = HashMap::new();
    for line in lines {
        let re = regex::Regex::new(
            r"Compte ENT (.+) \((.+)\) : nouvel utilisateur, login demandé : (.+)\.",
        )
        .unwrap();
        if let Some(caps) = re.captures(line) {
            let full_name = caps.get(1).unwrap().as_str().to_string();
            let ent_login = caps.get(2).unwrap().as_str().to_string();
            let asked_login = caps.get(3).unwrap().as_str().to_string();
            let pass = passwords.get(&asked_login);
            created_lines.insert(
                full_name.clone(),
                CreatedLine {
                    full_name,
                    ent_login,
                    asked_login,
                    password: pass.map(|s| s.to_string()),
                },
            );
        }
    }
    created_lines
}

/// Read passwords from se4 log lines.
///
/// ```text
/// Utilisateur crée : {{asked_login}}, type : Eleves, mot de passe initial : {{password}} .
/// ```
///
/// returning a login => password hashmap
fn read_passwords(lines: &[String]) -> HashMap<String, String> {
    let mut passwords = HashMap::new();
    for line in lines {
        let re = regex::Regex::new(
            r"Utilisateur crée : (.+), type : Eleves, mot de passe initial : (.+) \.",
        )
        .unwrap();
        if let Some(caps) = re.captures(line) {
            let login = caps.get(1).unwrap().as_str().to_string();
            let password = caps.get(2).unwrap().as_str().to_string();
            passwords.insert(login, password);
        }
    }
    passwords
}

/// Load a log file into a vector of strings.
pub fn load_log(path: &str) -> Vec<String> {
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    reader.lines().map(|l| l.unwrap()).collect()
}
